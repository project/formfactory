<?php

namespace Drupal\formfactory\Services;

use Drupal\kits\KitInterface;

/**
 * Interface FormFactoryInterface
 *
 * @package Drupal\formfactory
 */
interface FormFactoryInterface {

  public function load(array $form): FormFactoryInterface;

  public function setTree(bool $isTree = TRUE): FormFactoryInterface;

  public function getForm(): array;

  public function append(KitInterface $kit): FormFactoryInterface;

  public function attach(string $item): FormFactoryInterface;

  public function setDrupalSetting(string $namespace, string $key, string $value): FormFactoryInterface;
}
