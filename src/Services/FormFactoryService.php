<?php

namespace Drupal\formfactory\Services;

use Drupal\kits\KitInterface;

/**
 * Class FormFactoryService
 *
 * @package Drupal\formfactory
 */
class FormFactoryService implements FormFactoryInterface {

  private array $form;

  /**
   * @var KitInterface[]
   */
  private array $kits;

  public function load(array $form): FormFactoryInterface
  {
    $this->form = &$form;
    return $this;
  }

  public function setTree(bool $isTree = TRUE): FormFactoryInterface
  {
    $this->form['#tree'] = $isTree;
    return $this;
  }

  public function getForm(): array
  {
    $artifact = $this->form;
    foreach ($this->kits as $kit) {
      if ($kit->isChildrenGrouped()) {
        $artifact[$kit->getID()] = $kit->getArray();
        $artifact += $kit->getChildrenArray();
      }
      else {
        $artifact[$kit->getID()] = $kit->getArray();
      }
    }
    return $artifact;
  }

  public function append(KitInterface $kit): FormFactoryInterface
  {
    $this->kits[] = $kit;
    return $this;
  }

  public function attach(string $item, string $type = 'library'): FormFactoryInterface
  {
    $this->form['#attached'][$type][] = $item;
    return $this;
  }

  public function setDrupalSetting(string $namespace, string $key, string $value): FormFactoryInterface
  {
    $this->form['#attached']['drupalSettings'][$namespace][$key] = $value;
    return $this;
  }
}
